
#include "LeteciObjekat.h"
#include "Raketa.h"
#include "Lanser.h"

#define BROJ_RAKETA 100

void main()
{

	
	//unsigned int brojRaketa = 100;
	LeteciObjekat nizLetecihObjekata[BROJ_RAKETA];
	

	for (int i = 0; i < BROJ_RAKETA; i++)
	{
		nizLetecihObjekata[i] = Raketa();
	}

	Lanser lanser;

	for (int trenutnaRaketa = 0; trenutnaRaketa < BROJ_RAKETA; trenutnaRaketa++)
	{
		lanser.Ispali(nizLetecihObjekata, BROJ_RAKETA, trenutnaRaketa);
		
	}
}