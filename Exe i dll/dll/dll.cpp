#define DLL_API extern "C" __declspec(dllexport)
#define DLL_API_WO_EXTERN __declspec(dllexport)

#include "dll.h"

bool mojaKvadratnaFunkcija(int ulaz, int& izlaz)
{
	if (ulaz > maximumZaInteger || ulaz < -maximumZaInteger)
	{
		return false;
	}
	else
	{
		izlaz = ulaz*ulaz;
		return true;
	}
}

bool nekaDrugaKvadratnaFunkcija(int ulaz, int& izlaz)
{
	return true;
}
