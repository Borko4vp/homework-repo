#pragma once


#ifndef DLL_API
#define DLL_API extern "C" __declspec(dllimport)
#endif

#ifndef DLL_API_WO_EXTERN
#define DLL_API_WO_EXTERN __declspec(dllimport)
#endif


const int maximumZaInteger = 46340;


/*
name: mojaKvadratnaFunkcija
param1: int [in] broj ciji ce se kvadrat izracunati
param2: int [out] rezultat kvadrata
return bool: ukoliko je funkcija uspela vraca true, false u suprotnom
NOTE: nema interakcije sa u/i
*/

DLL_API bool mojaKvadratnaFunkcija(int ulaz, int& izlaz);

DLL_API_WO_EXTERN bool nekaDrugaKvadratnaFunkcija(int ulaz, int& izlaz);