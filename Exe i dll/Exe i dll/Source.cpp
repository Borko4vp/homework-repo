#include <stdio.h>
#include "dll.h"

/*
Primer sa import library-jem >> Implicitni load time linking  <<
*/

int main(int argc, char* argv[])
{
	int ulaz = 24;
	int rezultat = 0;
	if (mojaKvadratnaFunkcija(ulaz, rezultat))
	{
		printf("Kvadrat broja %d, je %d\n", ulaz, rezultat);
	}
	else
	{
		printf("Broj %d je van opsega\n", ulaz);
	}
	return 0;

	bool b = nekaDrugaKvadratnaFunkcija(ulaz, rezultat);
	
	return 0;
}