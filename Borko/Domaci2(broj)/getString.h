#pragma once

const int ReAllocStep = 10;	/* reAllocStep,step in which string will be reallocated,  lower values for longer strings may affect performance*/
const int MaxStringLengt = 150;  /*maxStringLengt to read*/
const int MaxNumLengt = 10;

char* getStringFromSTDIN(); //will return NULL if string not allocated properly from any reason


char* getCharSubString(char* inputString); //returns substring of the input string, will return NULL if string not allocated properly from any reason
char* getNumSubString(char* inputString); // returns Numeric substring of the input string, will return NULL if string not allocated properly from any reason
double numString2Number(char* numString); // returns the int value of the char represented number numString, returns -1 if failed
//char* inverseString(char* inputString); //returns the inverse string to an inputString