#include <stdio.h>
#include <malloc.h>
#include <math.h>

#include "getString.h"


char* getStringFromSTDIN()
{

	char* inputString = (char*)malloc((ReAllocStep+1)* sizeof(char));

	int currentBufferIndex = 0;
	char ch = '\0';

	while ((ch = fgetc(stdin)) != EOF && ch != '\n')
	{
		inputString[currentBufferIndex++] = ch;
		if (0 == currentBufferIndex % ReAllocStep)// if(inputString full)
		{
			if (currentBufferIndex >= MaxStringLengt - 1)
			{
				//input string longer than maxStringLengt, so dealocate currently allocated string,
				//set NULL for return string  and break
				inputString[currentBufferIndex] = '\0';
				delete[]inputString;
				inputString = NULL;
				break;
			}
			//reallocating string
			inputString = (char*)realloc((void*)inputString, (currentBufferIndex + ReAllocStep+1)*sizeof(char));
		}
	}
	if (NULL != inputString){
		inputString[currentBufferIndex] = '\0';
	}
	return inputString;
}
char* getCharSubString(char* inputString)
{
	char* inputStringCopy = inputString;
	int currentBufferIndex = 0, i = 0; 
	char charBuffer = '?';
	char* charSubString = new char[ReAllocStep+1];

	while ('\0' != (charBuffer=inputStringCopy[i++]))
	{
		if (charBuffer >= '0' && charBuffer <= '9');
		else
		{
			charSubString[currentBufferIndex++] = charBuffer;
		}

		if (0 == currentBufferIndex % ReAllocStep) //if (charSubString full)
		{
			//reallocating string
			charSubString = (char*)realloc((void*)charSubString, (currentBufferIndex + ReAllocStep+1)*sizeof(char));
		}
		
	}
	charSubString[currentBufferIndex] = '\0';
	if ('\0' == charSubString[0])
		return NULL;
	else
		return charSubString;

}
char* getNumSubString(char* inputString)
{
	char* inputStringCopy = inputString;
	int currentBufferIndex = 0, i = 0;
	char charBuffer = '?';
	char* numSubString = new char[ReAllocStep+1];

	while ('\0' != (charBuffer = inputStringCopy[i++]))
	{
		if (charBuffer >= '0' && charBuffer <= '9')
		{
			numSubString[currentBufferIndex++] = charBuffer;
		}

		if (0 == currentBufferIndex % ReAllocStep) //if (numSubString full)
		{
			//reallocating string
			numSubString = (char*)realloc((void*)numSubString, (currentBufferIndex + ReAllocStep+1)*sizeof(char));
		}
	}
	numSubString[currentBufferIndex] = '\0';

	if ('\0' == numSubString[0]) 
		return NULL;
	else  
		return numSubString;
}
double numString2Number(char* numString)
{
	int numStringValue = 0;
	int i = 0;
	char charBuffer = '\0';
	while ('\0' != (charBuffer = numString[i]))
	{
		if (charBuffer >= '0' && charBuffer <= '9')
		{
			
			numStringValue = (numString[i] - '0') + numStringValue*10;
			i++;
			if (i > MaxNumLengt)
			{
				numStringValue = -1;
				i = 0;
				break;
			}
		}
		else
		{
			numStringValue = -1;
			i = 0;
			break;
		}
	}
	return numStringValue;
}