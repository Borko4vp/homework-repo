#include <stdio.h>
#include <conio.h>
#include <math.h>

#include "getString.h"


int main()
{
	printf("Molim, unesite neki broj:");
	char* inputString = getStringFromSTDIN();
	
	if (inputString)
	{
		if ('\0' == inputString[0]){ printf("Unet je prazan string!\n"); }
		else 
		{
			char* charSubString = getCharSubString(inputString);
			char* numSubString = getNumSubString(inputString);
			if (numSubString)
			{
				if (charSubString)
					printf("Niz koji ste uneli sadrzi nenumericke karaktere,\n numericki deo izgleda: [%s]\n ", numSubString);
			
				double numSubStringValue = numString2Number(numSubString);
				if (0 <= numSubStringValue)
				{
					printf("Broj koji ste uneli je : %.2f\n", numSubStringValue);
					printf("kvadratni koren broja koji ste uneli je : %.2f\n", sqrt(numSubStringValue));
				}
				else
				{
					printf("Preveliki niz brojeva. MaxNumLengt = %d\n", MaxNumLengt);
				}
			
			}
			else 
			{
				printf("Niz koji ste uneli ne sadrzi ni jednu cifru!!\n");
			}
		}
		//printf("Uneti string je:  %s\n", inputString);	
		//printf("\n %s \n", inverseString(inputString));
	}
	else 
	{
		printf("Preveliki ulazni string. MaxStringLengt = %d\n", MaxStringLengt);
	}

	
	_getch();


}