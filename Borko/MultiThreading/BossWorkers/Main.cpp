#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <process.h>

bool syncFlag = false;
CRITICAL_SECTION criticalSection;

unsigned int __stdcall worker2Func(void* worker1Handle)
{
	while (!syncFlag);
	return 0;
}

unsigned int __stdcall worker1Func(void*)
{
	printf("cekam na enter....");
	while ('\n' != fgetc(stdin))
	{
	}

	EnterCriticalSection(&criticalSection);
	syncFlag = true;
	LeaveCriticalSection(&criticalSection);

	return 0;
}


void main()
{
	InitializeCriticalSection(&criticalSection);
	HANDLE worker1Thread = (HANDLE)_beginthreadex(NULL, 0, worker1Func, NULL, 0, NULL);
	HANDLE worker2Thread = (HANDLE)_beginthreadex(NULL, 0, worker2Func, worker1Thread, 0, NULL);


	WaitForSingleObject(worker1Thread,INFINITE);
	WaitForSingleObject(worker2Thread, INFINITE);

	DeleteCriticalSection(&criticalSection);
}