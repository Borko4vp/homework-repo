#pragma once

#include <condition_variable>

typedef struct pData {
	int op1;
	int op2;
	char opCode;

	uintptr_t myThreadHandle;
	unsigned int myThreadID;
	double result;
	HANDLE argRdy;
} MYDATA, *PMYDATA;
