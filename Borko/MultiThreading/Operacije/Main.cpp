#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <process.h>
#include "header.h"

unsigned int __stdcall calcFunction(void*  argData)
{
	PMYDATA inData = (PMYDATA)argData;
	WaitForSingleObject(inData->argRdy, INFINITE);
	switch (inData->opCode)
	{
		case'+': inData->result = inData->op1 + inData->op2; break;
		case'-': inData->result = inData->op1 - inData->op2; break;
		case'*': inData->result = inData->op1 * inData->op2; break;
		case'/': inData->result = inData->op1 / inData->op2; break;
		case'%': inData->result = inData->op1 % inData->op2; break;
	}
	HANDLE threadHandle = (HANDLE)inData->myThreadHandle;
	printf(" Thread 0x %x returned : %.2f\n", inData->myThreadID, inData->result);
	return (unsigned int)inData->result;
}
void main()
{
	PMYDATA* argDataArray = new PMYDATA[10];
	PMYDATA argData = new MYDATA;
	int cnt = 0;
	while (cnt < 10)
	{
		argDataArray[cnt] = new MYDATA;
		argDataArray[cnt]->argRdy = CreateSemaphore(NULL, 0, 1, NULL);
		argDataArray[cnt]->myThreadHandle = _beginthreadex(NULL, 0, calcFunction, argDataArray[cnt], 0, &(argDataArray[cnt]->myThreadID));
		cnt++;
	}
	argData->op1 = 0;
	argData->op2 = 0;
	argData->opCode = '\0';
	char ch = '\0';
	char* ops = new char[10];
	int i = 0;

	printf("Unesite prvi broj: ");
	scanf_s("%d", &argData->op1);
	printf("Unesite drugi broj: ");
	scanf_s("%d", &argData->op2);
	fgetc(stdin);
	printf("Unesite niz operacija po zelji (/ * + - %):");
	int opCnt = 0;
	while ('\n' != (ch = fgetc(stdin)) || 10 == opCnt)
	{
		argData->opCode = ch;
		//argDataArray[cnt] = new MYDATA; //- this line is depraceted as it was usefull for the fist version of homework 
		argDataArray[opCnt]->op1 = argData->op1;
		argDataArray[opCnt]->op2 = argData->op2;
		argDataArray[opCnt]->opCode = argData->opCode;
		//user input data for the first operation thread is ready startig from here


		ReleaseSemaphore(argDataArray[opCnt]->argRdy, 1, NULL);
		//argDataArray[i]->myThreadHandle = _beginthreadex(NULL, 0, calcFunction, argDataArray[i], 0, &(argDataArray[i]->myThreadID));

		//in the first version of the homework the threads were created when user input was ready,
		//which is changed to creating threds at the start and locking them on userinput data semaphore,
		//and then release user data semaphor on the place where that data is rdy
		
		opCnt++;

	}
	_getch();
}
