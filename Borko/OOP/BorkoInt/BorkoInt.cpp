#include "BorkoInt.h"


int BorkoInt::getValue()
{
	if (NULL != this->value)
		return *value;
	else return 0;
}

void BorkoInt::setValue(int eulav)
{
	if (NULL != this->value)
		*value = eulav;
}
BorkoInt::BorkoInt()
{
	if (NULL == this->value)
		this->value = new int();
	*value = 0;
}
BorkoInt::BorkoInt(int  rhs)
{
	if (NULL == this->value)
		this->value = new int();
	*value = rhs;
}
BorkoInt::BorkoInt(const BorkoInt& rhs)
{
	if (NULL == this->value)
		this->value = new int();
	
	*(this->value) = *(rhs.value);
}
BorkoInt::BorkoInt(const BorkoInt* rhs)
{
	if (NULL == this->value)
		this->value = new int();

	*(this->value) = *(rhs->value);
}
BorkoInt*  BorkoInt::operator=(const BorkoInt& rhs)
{
	if (this != &rhs)
	{
		if (NULL == this->value)
			this->value = new int();

		*(this->value) = *(rhs.value);
	}

	return this;
}
BorkoInt* BorkoInt::operator+( BorkoInt& rhs)
{
	if (NULL != this->value)
	{
		BorkoInt* tmp = new BorkoInt();
		tmp->setValue(this->getValue() + rhs.getValue());
		return tmp;
	}
}
BorkoInt* BorkoInt::operator-( BorkoInt& rhs)
{
	if (NULL != this->value)
	{
		BorkoInt* tmp = new BorkoInt();
		tmp->setValue(this->getValue() - rhs.getValue());
		return tmp;
	}
}
BorkoInt* BorkoInt::operator ++(int)
{
	BorkoInt* tmp = new BorkoInt();
		tmp->setValue(this->getValue());
	//this->setValue(tmp + 1);
	++*this;
	return tmp;
}

BorkoInt* BorkoInt::operator++()
{
	this->setValue(*(this->value) +1);
	return this;
}

int BorkoInt::operator()(BorkoInt&)
{

	return this->getValue();
}