#pragma once
#include <stdio.h>


class BorkoInt
{
	int* value;

public:

	int getValue();
	void setValue(int);

	BorkoInt();
	BorkoInt(int);
	BorkoInt(const BorkoInt& rhs);
	BorkoInt(const BorkoInt* rhs);

	BorkoInt*  operator=(const BorkoInt& rhs);

	BorkoInt* operator+ ( BorkoInt& rhs);
	BorkoInt* operator- ( BorkoInt& rhs);

	BorkoInt* operator ++(int);
	BorkoInt* operator++();

	int operator()(BorkoInt&);


};