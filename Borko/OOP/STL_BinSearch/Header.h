#pragma once


template <typename T> const T binSearchAscending(std::vector<T> niz, const T key);

template <typename T> const T binSearchAscending(std::vector<T> niz, const T key)
{
	int i = 0, j = niz.size();

	while (i < j)
	{
		if (niz[(i + j) / 2] > key)
			j = (i + j) / 2;
		else if (niz[(i + j) / 2] == key)
			return niz[(i + j) / 2];
		else if (niz[(i + j) / 2] < key)	
			i = (i + j) / 2;
	}
	return 0;
}
