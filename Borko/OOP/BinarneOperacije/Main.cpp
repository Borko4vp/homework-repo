#include <stdio.h>
#include <thread>
#include "BinarnaOperacija.h"

BinarnaOperacija* factory(char opCode)
{
	switch (opCode)
	{
	case'+': return new Sabiranje(); break;
	case'-': return new Oduzimanje(); break;
	case'*': return new Mnozenje(); break;
	case'/': return new Deljenje(); break;
	default: printf("nepoznata operacija"); break;
	}

}
void main()
{
	int opCnt = 0;
	char ch = 0;

	BinarnaOperacija** operacije = new BinarnaOperacija*;

	printf("Unesite niz operacija po zelji (/ * + -):");
	while ('\n' != (ch = fgetc(stdin)) || 10 == opCnt)
	{
		operacije[opCnt] = factory(ch);

		opCnt++;
	}
	int  opCntMax = opCnt;
	
	while (--opCnt >= 0)
	{

		pData* argData = new pData;
		argData->argRdy = CreateSemaphore(NULL, 0, 1, NULL);
		argData->op1 = opCnt;
		argData->op2 = opCnt;
		
		ReleaseSemaphore(argData->argRdy, 1, NULL);
		operacije[opCnt]->setArgData(argData);
		
		//BorkoThread* pThread = new BorkoThread(&Sabiranje::izvrsiOperaciju,static_cast<void*>(operacije[opCnt]));
		std::thread t(&BinarnaOperacija::izvrsiOperaciju, operacije[opCnt]);
		t.join();
		//printf(" %f \n",operacije[opCnt]->izvrsiOperaciju(argData));

	}


}