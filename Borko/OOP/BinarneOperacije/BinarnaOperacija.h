#pragma once

#include "BorkoThread.h"


class BinarnaOperacija
{
protected:	
	pData* argData;

public: 
	virtual DWORD WINAPI izvrsiOperaciju() = 0;
	void setArgData(pData* argData);

};

class Sabiranje:public BinarnaOperacija
{

public:
	virtual DWORD WINAPI izvrsiOperaciju();

};
class Oduzimanje :public BinarnaOperacija
{

public:
	virtual DWORD WINAPI izvrsiOperaciju();

};
class Deljenje :public BinarnaOperacija
{

public:
	virtual DWORD WINAPI izvrsiOperaciju();

};
class Mnozenje :public BinarnaOperacija
{

public:
	virtual DWORD WINAPI izvrsiOperaciju();

};