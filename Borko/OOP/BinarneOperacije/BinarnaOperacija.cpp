#include "BinarnaOperacija.h"


void BinarnaOperacija::setArgData(pData* argData)
{
	this->argData = argData;
}


DWORD WINAPI Sabiranje::izvrsiOperaciju()
{
	WaitForSingleObject(this->argData->argRdy, INFINITE);
	
	this->argData->result = this->argData->op1 + this->argData->op2;
	return (DWORD)this->argData->result;
}

DWORD WINAPI Oduzimanje::izvrsiOperaciju()
{
	WaitForSingleObject(this->argData->argRdy,INFINITE);
	this->argData->result = this->argData->op1 - this->argData->op2;
	return (DWORD)this->argData->result;
}

DWORD WINAPI Deljenje::izvrsiOperaciju()
{

	WaitForSingleObject(this->argData->argRdy, INFINITE);
	this->argData->result = this->argData->op1 / this->argData->op2;
	return (DWORD)this->argData->result;
}

DWORD WINAPI Mnozenje::izvrsiOperaciju()
{

	WaitForSingleObject(this->argData->argRdy, INFINITE);
	this->argData->result = this->argData->op1 * this->argData->op2;
	return (DWORD)this->argData->result;
}