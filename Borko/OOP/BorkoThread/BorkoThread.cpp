#include "BorkoThread.h"



BorkoThread::BorkoThread(DWORD(WINAPI *start_address)(void* p), void* argList)
{
	InitializeCriticalSection(&(this->criticalSection));


	mySemHandle = CreateSemaphore(NULL, 0, 1, NULL);
	this->setMyThreadState(SUSPENDED);
	EnterCriticalSection(&(this->criticalSection));
	 //(HANDLE)_beginthreadex(NULL, 0, start_address, pArg, 0, &(this->myThreadID)); 
	this->myThreadHandle = CreateThread(NULL, 0, start_address, argList, CREATE_SUSPENDED, this->myThreadID);
	//SuspendThread(myThreadHandle);
	LeaveCriticalSection(&(this->criticalSection));
	
}
void BorkoThread::setMyThreadState(THREAD_STATE threadState)
{
	EnterCriticalSection(&(this->criticalSection));
	this->myThreadState = threadState;
	LeaveCriticalSection(&(this->criticalSection));
}
void BorkoThread::StartThread()
{
	this->setMyThreadState(RUNNING);
	ResumeThread(myThreadHandle);
	
}

int BorkoThread::StopThread()
{
	if (RUNNING == this->myThreadState)
	{
		TerminateThread(myThreadHandle,TERMINATED);
		this->setMyThreadState(TERMINATED);
		return 1;
	}
	return 0;
}
bool BorkoThread::isStopSignaled()
{
	return this->myThreadState == TERMINATED;
}

BorkoThread::~BorkoThread()
{
	DeleteCriticalSection(&(this->criticalSection));
}
LPDWORD BorkoThread::getThreadID() const
{
	return this->myThreadID;
}
HANDLE BorkoThread::getMyThreadHandle()const 
{
	return this->myThreadHandle;
}

pData* BorkoThread::getArgs()
{
	return this->pArg;
}
void BorkoThread::Wait(HANDLE h, int time)
{
	WaitForSingleObject(h, time);

}