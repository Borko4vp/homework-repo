#include <windows.h>
#include <process.h>


enum THREAD_STATE { UNKNOWN=0, READY, RUNNING, SUSPENDED, TERMINATED };

typedef struct pData {
	int op1;
	int op2;
	double result;
	char opCode;
	HANDLE argRdy;
} MYDATA, *PMYDATA;

class BorkoThread
{
	HANDLE myThreadHandle;
	LPDWORD myThreadID;
	THREAD_STATE myThreadState;

	pData* pArg;

	HANDLE mySemHandle;
	CRITICAL_SECTION criticalSection;


	void setMyThreadState(THREAD_STATE);
public:
	BorkoThread(DWORD(WINAPI *start_address)(void* p), void* argList);

	void Wait(HANDLE, int);
	void StartThread();
	int StopThread();
	bool BorkoThread::isStopSignaled();
	pData* getArgs();
	THREAD_STATE getWorkStatus()const{ return myThreadState; }
	LPDWORD BorkoThread::getThreadID() const;
	HANDLE BorkoThread::getMyThreadHandle() const;
	~BorkoThread();
};