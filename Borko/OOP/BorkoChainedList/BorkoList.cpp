#include <stdio.h>
#include "BorkoList.h"


BorkoList::BorkoList(bool Type)
{
	head = nullptr;
}

void BorkoList::Insert(char charToInsert)
{
	SCListNode* newNode = new SCListNode(charToInsert);
	if (nullptr == this->head)
	{
		head = newNode;
	}
	else
	{
		SCListNode* pom = this->head;
		while (nullptr != pom->next())
		{


			pom = pom->next();
		}
		pom->setNext(newNode);
	}
}
bool BorkoList::Remove(char charToRemove)
{

	if (nullptr == head)return false;
	SCListNode* pom = this->head;
	SCListNode* toBeRemoved = nullptr;
	if (charToRemove == pom->getValue())
	{
		this->head = pom->next();
		return true;
	}
	else{
		while (nullptr != pom->next())
		{
			if (charToRemove == pom->next()->getValue())
			{
				toBeRemoved = pom->next();
				pom->setNext(pom->next()->next());
				delete toBeRemoved;
				return true;
			}

			pom = pom->next();
		}
		return false;
	}
	
}
bool BorkoList::Remove(int indexOfCharToRemove)
{
	if (nullptr == head)return false;
	SCListNode* pom = this->head;
	SCListNode* toBeRemoved = nullptr;
	int i = 1;
	if (0 == indexOfCharToRemove) return false;
	if (i == indexOfCharToRemove)
	{
		this->head = pom->next();
		delete pom;
		return true;
	}
	else{
		while (i < indexOfCharToRemove-1)
		{
			
			pom = pom->next();
			i++;
		}
		toBeRemoved = pom->next();
		pom->setNext(toBeRemoved->next());
		delete toBeRemoved;
		return true;
	}
}
void BorkoList::EmptyList()
{
	while (nullptr != this->head)
	{
		SCListNode* pom = this->head;
		this->head = this->head->next();
		delete pom;
	}
}
int Find(char charToFind);
void BorkoList::Print(bool forward)
{
	SCListNode* pom = this->head;
	if (true == forward){
		while (nullptr != pom)
		{
			printf("%c", pom->getValue());
			pom = pom->next();
		}
		printf("\n");
	}
}

BorkoList::~BorkoList(){}