#pragma once
class SCListNode{

protected:
	char value;
	SCListNode* pNext;

public:
	inline SCListNode(char value){
		this->value = value;
		this->pNext = nullptr;
	}
	inline char getValue(){ return this->value; }
	inline void setValue(char newValue){ this->setValue(newValue); }
	inline SCListNode* next(){ return this->pNext; }
	inline void setNext(SCListNode* newNext){ this->pNext = newNext; }

};
class DCListNode :public SCListNode{

protected:
	DCListNode* pPrev;

public:
	inline DCListNode(char value) :SCListNode(value){
		this->pPrev=nullptr;
	}
	inline DCListNode* prev(){ return this->pPrev; }

};

class BorkoList
{
	

private:
	SCListNode* head;

public:
	BorkoList(bool Type);


	void Insert(char charToInsert);
	bool Remove(char charToRemove);
	bool Remove(int indexOfCharToRemove);
	void EmptyList();
	int Find(char charToFind);
	void Print(bool forward = true);

	~BorkoList();


};