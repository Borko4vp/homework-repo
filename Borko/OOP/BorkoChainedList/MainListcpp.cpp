
#include "BorkoList.h"

void main()
{
	BorkoList* borkoList = new BorkoList(false);

	borkoList->Insert('C');
	borkoList->Print();
	borkoList->Insert('i');
	borkoList->Print();
	borkoList->Insert('k');
	borkoList->Print();
	borkoList->Insert('a');
	borkoList->Print();
	borkoList->Insert('_');
	borkoList->Print();
	borkoList->Insert('A');
	borkoList->Print();
	borkoList->Insert('C');
	borkoList->Print();
	borkoList->Insert('A');
	borkoList->Print();

	borkoList->EmptyList();

	borkoList->Remove(1);
	borkoList->Print();
	borkoList->Remove(2);
	borkoList->Print();
	borkoList->Remove(6);
	borkoList->Print();
	borkoList->Remove('_');
	borkoList->Print();
}