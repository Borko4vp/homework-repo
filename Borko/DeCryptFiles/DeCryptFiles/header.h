#pragma once

void plus(int prvi, int drugi, int& rezultat);
void minus(int prvi, int drugi, int& rezultat);
void puta(int prvi, int drugi, int& rezultat);
void podeljeno(int prvi, int drugi, int& rezultat);
void moduo(int prvi, int drugi, int& rezultat);
int operacija(int prvi, int drugi, char operacija);

