#include <thread>
#include "Header.h"

int main()
{
	int prvi = 0;
	int drugi = 0;
	printf("Molim unesite prvi broj\n");
	scanf("%d", &prvi);
	printf("Molim unesite drugi broj\n");
	scanf("%d", &drugi);
	printf("Molim unesite operacije\n");
	char operacije[10] = { 0 };
	scanf("%s", operacije);
	for (unsigned int i = 0; i < strlen(operacije); i++)
	{
		int rezultat = 0;
		switch (operacije[i])
		{
		case '+':
		{
					std::thread t1(plus, prvi, drugi, std::ref(rezultat));
					t1.join();
		}
			break;
		case '-':
		{
					std::thread t1(minus, prvi, drugi, std::ref(rezultat));
					t1.join();
		}
			break;
		case '*':
		{
					std::thread t1(puta, prvi, drugi, std::ref(rezultat));
					t1.join();
		}
			break;
		case '/':
		{
					std::thread t1(podeljeno, prvi, drugi, std::ref(rezultat));
					t1.join();
		}
			break;
		case '%':
		{
					std::thread t1(moduo, prvi, drugi, std::ref(rezultat));
					t1.join();
		}
			break;
		}
		printf("Rezultat operacije %c je %d\n", operacije[i], rezultat);
	}

	return 0;
}