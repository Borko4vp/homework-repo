#include "Header.h"

void plus(int prvi, int drugi, int& rezultat)
{
	char aritmetickaOperacija = '+';
	rezultat = operacija(prvi, drugi, aritmetickaOperacija);
}

void minus(int prvi, int drugi, int& rezultat)
{
	char aritmetickaOperacija = '-';
	rezultat = operacija(prvi, drugi, aritmetickaOperacija);
}

void puta(int prvi, int drugi, int& rezultat)
{
	char aritmetickaOperacija = '*';
	rezultat = operacija(prvi, drugi, aritmetickaOperacija);
}

void podeljeno(int prvi, int drugi, int& rezultat)
{
	char aritmetickaOperacija = '/';
	rezultat = operacija(prvi, drugi, aritmetickaOperacija);
}

void moduo(int prvi, int drugi, int& rezultat)
{
	char aritmetickaOperacija = '%';
	rezultat = operacija(prvi, drugi, aritmetickaOperacija);
}

int operacija(int prvi, int drugi, char operacija)
{
	int rezultat = 0;
	switch (operacija)
	{
	case '+':
		rezultat = prvi + drugi;
		break;
	case '-':
		rezultat = prvi - drugi;
		break;
	case '*':
		rezultat = prvi * drugi;
		break;
	case '/':
		if (drugi != 0)
		{
			rezultat = prvi / drugi;
		}
		break;
	case '%':
		if (drugi != 0)
		{
			rezultat = prvi % drugi;
		}
		break;
	default:
		break;
	}
	return rezultat;
}