#pragma once
#include <stdio.h>

/*#ifndef LOG_API
#define LOG_API extern "C" __declspec(dllimport)
#endif //LOG_API*/
class __declspec(dllexport) Loger
{
	FILE* fileHandle;
public:
	Loger(char* fileName);
	void initLoger(char* fileName);
	bool log(char* function, int line, char* comment);
};