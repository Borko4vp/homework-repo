#pragma once
enum opeCode { AND=1, OR=2, XOR=3};


const int ReAllocStep = 2; //reallocation step used for dinamic allocation of memory for file

/*Function which encrypts each char in a file with the parameter char key and operation opCode*/
char*** doCharsWithKey(char*** inputArray, char key, opeCode opCode);

/*Function used to read file and add it to the appropriate structure
parameter1: FILE* is the file handle to an file open with reading priviliges
parameter2: char**** is the reference to a char*** pointer, which will be the pointer to the file structure after this function is done
return 0 if sucess,  and -1 if failed*/
int readFile(FILE*,char****);

/*Function used to write to file from the structure in memory made by readFile(FILE*,char****) function
parameter1: FILE* is the file handle to an file open with writing priviliges 
parameter2: char*** is the pointer to a structure which will be wrote to a file
returns 0 if succes, and -1 if failed*/
int write2File(FILE*,char***);

/*Below are methods for manipulating the string structure made by readFile(FILE*,char****) function*/

/*this method inverse the order of Rows in a file, first line becomes last, and the last one becomes first, etc.*/
char*** inverseRows(char***);

/*this method inverse the order of Words in a Row, first word becomes last, and the last one becomes first in a row, etc.
receives char** which simulates the pointer to a Row, and returns changed pointer*/
char** inverseWords(char**);

/*this method inverse the order of Chars(letters) in a Word, first letter becomes last, and the last one becomes first in a word, etc.
receives char* which simulates the pointer to a Word, and returns changed pointer*/
char* inverseChars(char*);

/*this method inverse the order of Words and Chars(letters) in a Row, so basicaly do inverseWords() and inverseChars() for all words in a Row
receives char** which simulates the pointer to a Row, and returns changed pointer*/
char** inverseWholeRow(char**);

/*this method inverse the order of Rows, Words and Chars(letters) in a whole File, so basicaly do inverseRows(),
inverseWords() for all rows in file, and inverseChars() for all words in a Row
receives char*** which simulates the pointer to a File, and returns changed pointer*/
char*** inverseWholeFile(char***);

/*this method  calls inverseWholeRow(char**) method for all Rows in a file
receives char*** which simulates the pointer to a File, and returns changed pointer*/
char*** inverseAllRows(char***);
/*this method  calls inverseChars(char*) method for all Words in a file
receives char*** which simulates the pointer to a File, and returns changed pointer*/
char*** inverseAllChars(char***);
