#include <stdio.h>
#include <malloc.h>

#include "File.h"



char*** doCharsWithKey(char*** inputArray, char key, opeCode opCode)
{
	if (!inputArray) return NULL;
	int i = 0, j = 0, k=0;
	while ('\0' != inputArray[i])
	{
		j = 0;
		while ('\0' != inputArray[i][j])
		{
			k = 0;
			while ('\0' != inputArray[i][j][k])
			{
				switch (opCode)
				{
				case 1: inputArray[i][j][k] = inputArray[i][j][k] & key; break;
				case 2: inputArray[i][j][k] = inputArray[i][j][k] | key; break;
				case 3: inputArray[i][j][k] = inputArray[i][j][k] ^ key; break;
				}
				
				k++;
			}
			j++;
		}
		i++;
	}
	return inputArray;
}
char* inverseChars(char* inputString)
{
	int i = 0, maxIndex = 0;
	char* outputString = NULL;
	if (!inputString)
		return outputString;

	while ('\0' != inputString[i])
	{
		i++;
	}
	if (0 <= i)
	{
		maxIndex = i;
		outputString = new char[i];
		while (0 <= i)
		{
			outputString[maxIndex - i] = inputString[i - 1];
			i--;
		}
	}
	outputString[maxIndex] = '\0';
	return outputString;
}
char** inverseWords(char** inputArray)
{
	int i = 0, maxIndex = 0;
	char** outputArray = NULL;
	if (!inputArray)
		return outputArray;

	while ('\0' != inputArray[i])
	{
		i++;
	}
	if (0 <= i)
	{
		maxIndex = i;
		outputArray = new char*[i];
		while (0 <= i)
		{
			outputArray[maxIndex - i] = inputArray[i - 1];
			i--;
		}
	}
	outputArray[maxIndex] = '\0';
	return outputArray;
}
char*** inverseRows(char*** inputArray)
{
	int i = 0, maxIndex = 0;
	char*** outputArray = NULL;
	if (!inputArray)
		return outputArray;

	while ('\0' != inputArray[i])
	{
		i++;
	}
	if (0 <= i)
	{
		maxIndex = i;
		outputArray = new char**[i];
		while (0 <= i)
		{
			outputArray[maxIndex-i] = inputArray[i-1];
			i--;
		}
	}
	outputArray[maxIndex] = '\0';
	return outputArray;
}
char** inverseWholeRow(char** inputRow)
{
	if (!inputRow) return NULL;
	int i = 0;
	while (inputRow[i] != '\0')
	{
		inputRow[i] = inverseChars(inputRow[i]);
		i++;
	}
	inputRow = inverseWords(inputRow);
	return inputRow;
}
char*** inverseWholeFile(char*** inputArray)
{
	if (!inputArray) return NULL;
	int i = 0;
	while (inputArray[i] != '\0')
	{
		inputArray[i] = inverseWholeRow(inputArray[i]);
		i++;
	}
	inputArray = inverseRows(inputArray);
	return inputArray;
}
char*** inverseAllRows(char*** inputArray)
{
	if (!inputArray) return NULL;
	int i = 0;
	while ('\0' != inputArray[i])
	{
		inputArray[i] = inverseWholeRow(inputArray[i]);
		i++;
	}
	return inputArray;
}
char*** inverseAllChars(char*** inputArray)
{
	if (!inputArray) return NULL;
	int i = 0, j = 0;
	while ('\0' != inputArray[i])
	{
		j = 0;
		while ('\0' != inputArray[i][j])
		{
			inputArray[i][j] = inverseChars(inputArray[i][j]);
			j++;
		}
		i++;
	}
	return inputArray;
}
int readFile(FILE* fileHandle, char**** string)
{
	if (!fileHandle) return -1;
	int currentRowIndex = 0, currentWordIndex = 0, currentCharIndex = 0;
	*string = new char**[ReAllocStep]{};
	char*** local_string = *string;
	char ch;

	do
	{
		currentWordIndex = 0;
		if (0 == currentRowIndex % (ReAllocStep - 1))
		{
			local_string = (char***)realloc((void*)local_string, (currentRowIndex + ReAllocStep + 1)*sizeof(char***));
			for (int i = currentRowIndex; i < currentRowIndex + ReAllocStep + 1;i++)
				local_string[i] = NULL;
		}
		do
		{
			currentCharIndex = 0;
			if (0 == currentWordIndex % (ReAllocStep - 1))
			{
				if (!local_string[currentRowIndex])
					local_string[currentRowIndex] = new char*[ReAllocStep]{};
				else
				{
					local_string[currentRowIndex] = (char**)realloc((void*)(local_string[currentRowIndex]), (currentWordIndex + ReAllocStep + 1)*sizeof(char**));
					for (int i = currentWordIndex; i < currentWordIndex + ReAllocStep + 1; i++)
						local_string[currentRowIndex][i] = NULL;
				}
			}
			do
			{
				if (0 == currentCharIndex % (ReAllocStep - 1))
				{
					if (!local_string[currentRowIndex][currentWordIndex]) 
						local_string[currentRowIndex][currentWordIndex] = new char[ReAllocStep]{};
					else
					{
						local_string[currentRowIndex][currentWordIndex] = (char*)realloc((void*)(local_string[currentRowIndex][currentWordIndex]), (currentCharIndex + ReAllocStep + 1)*sizeof(char*));
						for (int i = currentCharIndex; i < currentCharIndex + ReAllocStep + 1;i++)
							local_string[currentRowIndex][currentWordIndex][i] = NULL;
					}
				}
				
				ch = fgetc(fileHandle);
				if ('\0' == ch) continue;
				if ((' ' != ch) && ('\n' != ch) && (EOF != ch)){
					local_string[currentRowIndex][currentWordIndex][currentCharIndex] = ch;
					currentCharIndex++;
				}
			} while ((' ' != ch) && ('\n' != ch)  && (EOF != ch));
			local_string[currentRowIndex][currentWordIndex][currentCharIndex] = '\0';
			currentWordIndex++;
			if (EOF == ch) break;
		} while ('\n' != ch);
		local_string[currentRowIndex][currentWordIndex] = '\0';
		currentRowIndex++;
	} while (EOF != ch);
	local_string[currentRowIndex] = '\0';
	*string = local_string;
	return 0;
}
int write2File(FILE* fileHandle,char*** string)
{
	if (!fileHandle) return -1;
	int currentRowIndex = 0, currentWordIndex = 0, currentCharIndex = 0;
	while (string[currentRowIndex])
	{
		currentWordIndex = 0;
		while (string[currentRowIndex][currentWordIndex])
		{
			currentCharIndex = 0;
			while (string[currentRowIndex][currentWordIndex][currentCharIndex])
			{
				fputc(string[currentRowIndex][currentWordIndex][currentCharIndex], fileHandle);
				currentCharIndex++;
			}
			fputc(' ', fileHandle);
			currentWordIndex++;
		}
		fputc('\n', fileHandle);
		currentRowIndex++;
	}
	return 0;
}