#include<stdio.h>

#include "File.h"

char* inputFile = "fajl.txt";
char* outputFile = "fajl_.txt";

void main(int argc, char* argv[])
{
	//Reading the input file
	FILE* file;
	fopen_s(&file, inputFile, "r");
	char*** string = new char**;
	if (0 == readFile(file, &string))
	{
		fclose(file);

		//manipulating the file
		int i = 1;
		while (i < argc)
		{
			switch (*(argv)[i++])
			{
			case'a':
			case'A':
			{
				string = inverseAllChars(string);
				break;
			}
			case'b':
			case'B':
			{
				string = inverseAllRows(string);
				break;
			}
			case'C':
			case'c':
			{
				string = inverseWholeFile(string);
				break;
			}
			case'D':
			case'd':
			{
				string = inverseRows(string);
				break;
			}
			case'E':
			case'e':
			{
				outputFile = inverseChars(inputFile);
				break;
			}

			}
		}
		///writing the changed string to output file
		fopen_s(&file, outputFile, "w");
		if (-1 == write2File(file, string))
			printf("Upis u fajl nije uspeo");
		fclose(file);
	}
	else
		printf("Citanje fajla nije uspelo");
	

}