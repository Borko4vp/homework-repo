#include <stdio.h>
#include <conio.h>
#include <math.h>



static char staticVar = 'z';
static char staticVar_2 = 'q';
static int staticVarI = 5; 

int main()
{
		
	char stackVar = 'c';
	char stackVar_2 = 'a';
	int stackVarI = 10;

	char* p_stackVar = &stackVar;
	char* p_stackVar_2 = &stackVar_2;
	int* p_stackVarI = &stackVarI;

	char* p_heapVar = new char('z');
	char* p_heapVar_2 = new char('q');

	char* p_staticVar = &staticVar;
	char* p_staticVar_2 = &staticVar_2;
	int* p_staticVarI = &staticVarI;
	/////////////////////////////////////////////////////

	printf(" %p\n %p\n", p_stackVar, p_stackVar_2);
	if (p_stackVar_2 - p_stackVar < 0)
		printf("Stack raste na dole (ka nizim adresama)!\n\n");
	else
		printf("Stack raste na gore (ka visim adresama)!\n\n");
	


	printf(" %p\n %p\n",p_heapVar, p_heapVar_2);
	if (p_heapVar_2 - p_heapVar < 0)
		printf("Heap raste na dole (ka nizim adresama)!\n\n");
	else
		printf("Heap raste na gore (ka visim adresama)!\n\n");


	printf(" %p\n %p\n", p_staticVar, p_staticVar_2);
	if (p_staticVar_2 - p_staticVar < 0)
		printf("Static memory raste na dole (ka nizim adresama)!\n\n");
	else
		printf("Static memory raste na gore (ka visim adresama)!\n\n");

	printf("Razlika izmedju staticke i stacka je %d bajtova\n", (int)(p_staticVar-p_stackVar));
	printf("Razlika izmedju staticke i stacka je %d integera(od po 4 bajta)\n", (int)(p_staticVarI - p_stackVarI));
	printf("Razlika izmedju staticke i stacka je %d bajtova\n", (int)((int)p_staticVarI - (int)p_stackVarI));
	//printf("Razlika izmedju heapa i stacka je %d bajtova\n", (int)(p_heapVar - p_stackVar));
	//printf("Razlika izmedju heapa i stacka je %d bajtova\n", (int)(p_heapVar_2 - p_stackVar_2));

	//printf("Razlika izmedju stacka i heapa je %d bajtova", abs(p_stackVar - p_heapVar));

	delete p_heapVar, p_heapVar_2;

	_getch();
}