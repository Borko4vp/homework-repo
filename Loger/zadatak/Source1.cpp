#include "Header.h"
#include <stdio.h>

void KoriscenjePrograma()
{
	printf("Program se koristi na sledeci nacin:\n");
	printf("zadatak.exe [opcije] [file]\n");
	printf("Opcije:\n");
	printf("c - logovanje u konzolu\nf - logovanje u fajl\n");
}

bool ParserKomandneLinije(int argc, char* argv[], KomandLajnArgumenti* komandLajnArgumenti)
{
	bool rezultat = false;
	switch (argc)
	{
	case 1:
	{
			  rezultat = false;
			  break;
	}
	case 2:
	{
			  if (argv[argc - 1][0] == 'c')
			  {
				  komandLajnArgumenti->tipLogovanja = KONZOLA;
				  rezultat = true;
			  }
			  break;
	}
	case 3:
	{
			  if (argv[argc - 2][0] == 'f')
			  {
				  komandLajnArgumenti->tipLogovanja = FAJL;
				  memcpy(komandLajnArgumenti->imeLogFajla, argv[2], strlen(argv[2]));
				  rezultat = true;
			  }
			  break;
	}
	}
	return rezultat;
}