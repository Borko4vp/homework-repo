#pragma once;
#include <windows.h>

enum TipLogovanja
{
	NEPOZNATO,
	KONZOLA,
	FAJL
};

struct KomandLajnArgumenti
{
	char imeLogFajla[MAX_PATH];
	TipLogovanja tipLogovanja;
};

void KoriscenjePrograma();

bool ParserKomandneLinije(int argc, char* argv[], KomandLajnArgumenti* komandLajnArgumenti);