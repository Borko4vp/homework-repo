#include <stdio.h>
#include <direct.h>
#include <Windows.h>

#define LOG_API extern "C" __declspec(dllexport)

#include "LogerImpl.h"

FILE* fp = NULL;

void inicijalizujLog(const char* imeLogFajla)
{
	if (imeLogFajla != nullptr)
	{
		char* trenutniFolder = nullptr;
		char celaPutanja[MAX_PATH] = { 0 };

		// Get the current working directory: 
		trenutniFolder = _getcwd(NULL, 0);
		if (trenutniFolder)
		{
			memcpy(celaPutanja, trenutniFolder, strlen(trenutniFolder));
			free(trenutniFolder);
			strcat(celaPutanja, "\\");
			strcat(celaPutanja, imeLogFajla);
		}
		fp = fopen(celaPutanja, "w+t");
	}
}

int logFunkcija(const char* mojKomentar, const char* imeFunkcije, long linija)
{
	if (mojKomentar)
	{
		fprintf(fp ? fp : stdout, "%s, %s, %d\n", mojKomentar, imeFunkcije, linija);
	}
	else
	{
		fprintf(fp ? fp: stdout, "%s, %d\n", imeFunkcije, linija);
	}
	return 0;
}
