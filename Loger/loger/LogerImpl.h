#pragma once

#ifndef LOG_API
#define LOG_API extern "C" __declspec(dllimport)
#endif //LOG_API


LOG_API int logFunkcija(const char* mojKomentar, const char* imeFunkcije, long linija);
LOG_API void inicijalizujLog(const char* logFile);
