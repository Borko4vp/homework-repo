#include "header.h"
#include <stdio.h>
#include <Windows.h>

int funkcijaSaUnusedParametrima(int prviBroj, int drugiBroj, int*)
{
	int rezultat = prviBroj + drugiBroj;
	return rezultat;
}

int statickaFunkcija()
{
	return 6;
}

int normalnaFunkcijaPozivaStaticku(int x)
{
	return x + statickaFunkcija();
}

void prikaziVreme(int x)
{
	printf("Proteklo je %d sekundi od ulaska u funkciju\n", x);
}

void otkucaj(vremenskiPokazivac vp)
{
	for (int i = 0; i < 10; i++)
	{
		vp(i);
		Sleep(1000);
	}
}


int& opasnaFunkcija()
{
	int x = 44;
	return x;
}

int obicnaFunkcija()
{
	int y = 55;
	return y;
}

int* funkcijaVracaPointer()
{
	int x = 66;
	return& x;
}

void funkcijaUzimaReferencu(int& x)
{
	x++;
}
void funkcijaUzimaPointer(int* x)
{
	(*x)--;
}

int funkcijaSaStatickimPromenljivim(int x)
{
	static int y = x;
	return y;
}

void printajBroj(int broj)
{
	if (broj > 9)
	{
		printajBroj(broj / 10);
	}
	printf("%d", broj % 10);
}
