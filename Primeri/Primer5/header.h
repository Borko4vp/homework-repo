#pragma once

void funkcija();			//Samo deklaracija funkcije - ukoliko je nigde ne pozovem niko nece da se buni

int funkcijaSaUnusedParametrima(int prviBroj, int drugiBroj, int* rezultat);

inline int inlineMax(int x, int y)
{
	return (x > y) ? x : y;
}

static int statickaFunkcija();

int normalnaFunkcijaPozivaStaticku(int x);

void prikaziVreme(int x);
typedef void (*vremenskiPokazivac)(int);
void otkucaj(vremenskiPokazivac);

void printajBroj(int broj);

int& opasnaFunkcija();
int obicnaFunkcija();

int* funkcijaVracaPointer();

void funkcijaUzimaReferencu(int& x);
void funkcijaUzimaPointer(int* x);

int funkcijaSaStatickimPromenljivim(int x);

