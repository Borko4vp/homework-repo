#include <stdio.h>
#include "header.h"

int statickaFunkcija()
{
	return 5; 
}

int main()
{
	printf("Funkcije\n");

	//Da sam ovde pozvao funkciju: funkcija();
	//ko bi se pobunio?

	int prviBroj = 4;
	int drugiBroj = 7;
	int rezultat = 0;
	int statickiPodatak = 0;

	int* pointerNaRezultat = &rezultat;
	int rezultatFunkcijeSaUnUsedParametrima = funkcijaSaUnusedParametrima(prviBroj, drugiBroj, pointerNaRezultat);

	int max = inlineMax(prviBroj, drugiBroj);	//U debug-u:
												//Debug Information Format:		C7 compatible(/ Z7)
												//Inline Function Expansion:	Only __inline (/Ob1)

	int nekaVrednost = statickaFunkcija();

	int nekaNovaVrednost = normalnaFunkcijaPozivaStaticku(statickaFunkcija());

	statickiPodatak = funkcijaSaStatickimPromenljivim(5);
	statickiPodatak = funkcijaSaStatickimPromenljivim(6);
	statickiPodatak = funkcijaSaStatickimPromenljivim(7);

	vremenskiPokazivac vp = prikaziVreme;		//callback
	otkucaj(vp);


	int nekiMojBroj = 123456789;				//rekurzija
	printajBroj(nekiMojBroj);

	int& rx = opasnaFunkcija();					//Primer loseg koriscenja reference na lokalni podatak
	int x = obicnaFunkcija();
	int y = rx;

	int z = 9;
	funkcijaUzimaReferencu(z);
	funkcijaUzimaPointer(&z);

	return 0;
}