#include "Header1.h"

/*
Primer naglasava scope - staticki externi (kao globalni)
i lokalni unutar funkcije i bloka
*/


int main()
{
	#pragma message(">>Primer 1 - Deklaracija definicija i oblast vazenja<<")
	int z = x;
	int rezultat = saberiBrojeve(x, z);
	dekrementirajX();
	dekrementirajX();
	z = x;
	dekrementirajY();
	dekrementirajY();
	dodelaUOblastiVazenja();
	z = y;
	char c = 'a';
	charPointer cp = &c;

	int niz[75] = {0}; 
	int _loseImeZaPromenljivu = 7;
	niz[33 + _loseImeZaPromenljivu] = 5;
	{
		int _novoLoseImeZaPromenljivu = _loseImeZaPromenljivu;
		_loseImeZaPromenljivu = 55;
		{
			int _loseImeZaPromenljivu = 33;
		}
	}
	
	return 0;
}
