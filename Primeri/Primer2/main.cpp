#include <stdio.h>
#include <limits>
using namespace std;

/*
Primer govori o tipovima i memorijama koje su zauzete za njega
*/


enum enumeracija
{	
	PRVI = 77,
	DRUGI,
	TRECI,
	CETVRTI
};

typedef unsigned int uInt;

int main()
{
	printf("Tipovi podataka - ugradjeni\n");
	//bool
	int boolVelicina = sizeof(bool);
	bool booleanValue = 9;							// Warning cast - static_cast<bool>(9)
	bool tacno = true;
	bool josTacnije = true;
	bool tacnoPlusJosTacnije = tacno + josTacnije;	// Warning  (tacno + josTacnije)!= 0; 
	bool tacnoIliJosTacnije = tacno | josTacnije;
	int x = true;
	int najveciBool = numeric_limits<bool>::max();
	//char
	int charVelicina = sizeof(char);
	int slovoA = 'A';
	int wcharVelicina = sizeof(wchar_t);
	wchar_t dugoSlovoA = L'a';
	int najveciChar = numeric_limits<char>::max();
	//int
	int obicanIntVelicina = sizeof(int);
	int kraciIntVelicina = sizeof(short);
	int duziIntVelicina = sizeof(long);
	int najduziIntVelicina = sizeof(__int64);
	int jedanOdNajduzihVelicina  = sizeof(long long);
	int brojUMemoriji = 300;
	int najveciInt = numeric_limits<int>::max();
	int najveciUnsignedInt = numeric_limits<unsigned>::max();

	//float/double/long double
	int floatVelicina = sizeof(float);
	int doubleVelicina = sizeof(double);
	int duziDoubleVelicina = sizeof(long double);

	float najveciFloat = numeric_limits<float>::max();
	double najveciDouble = numeric_limits<double>::max();
	long double najveciProsireniDouble = numeric_limits<long double>::max();
	
	int velicinaEnumericije = sizeof(enumeracija);
	int drugaVrednostUEnumeraciji = DRUGI;
	
	uInt neonznaceniInt = 0;

	return 0;
}