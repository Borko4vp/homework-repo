#include <stdio.h>
#include <string.h>
#include <assert.h>

int main()
{
	printf("Pointeri i reference\n");

	int* pointerNaInt = NULL;				//pointer na integer, inicijalizovan
	int* pointerNaIntPoNovomStandardu = nullptr;
	char** pointerNaPointerNaChar = NULL;	//pointer na pointer na char
	
	char* nekiPointer = "Ovo je string u statickoj memoriji";

	pointerNaPointerNaChar = &nekiPointer;

	printf("nekiPointer %s\n", nekiPointer);
	printf("Vrednost na drugoj poziciji je %c\n", nekiPointer[1]);
	printf("Vrednost nekog pointera preko pointera na pointera %s\n", *pointerNaPointerNaChar);
	
	const char* nekiNoviPointer = "Ovo je konstantni string i pravilnije ga referenciram nego >>nekiPointer<<";

	char ovoJeJosJedanNiz[] = { 'O', 'v', 'o', ' ', 'j', 'e', ' ', 's', 't', 'r', 'i', 'n', 'g', ' ', 'n', 'a', ' ',
		's', 't', 'e', 'k', 'o', 'v', 's', 'k', 'o', 'j', ' ', 'm', 'e', 'm', 'o', 'r', 'i', 'j', 'i'}; 
	
	//nekiPointer[4] = ovoJeJosJedanNiz[3];  //-> Sta je sa ovim redom kompajlira se, ali...???
	
	int vel = strlen(ovoJeJosJedanNiz); //sta mislite o velicini niza ovoJeJosJedanNiz
	//kako bolje inicijalizovati char niz
	int vel1 = sizeof(ovoJeJosJedanNiz);

	//dokaz da je ime niza pokazivac na prvi element niza
	char* pointerNaovoJeJosJedanNiz = ovoJeJosJedanNiz;
	char* pointerNaPrviElementovoJeJosJedanNiz = &ovoJeJosJedanNiz[0];
	void* voidPointer = &ovoJeJosJedanNiz;
		
	//pointerska aritmetika
	int intNiz[4] = {1, 2, 3, 4};
	char charNiz[4] = {'1', '2', '3', '4'};
	for(int i = 0; i < 4; i++)
	{
		printf("Adresa int elementa je %p\nAdresa char elementa je %p\n\n", &intNiz[i], &charNiz[i]);
	}

	int noviIntNiz[2] = {1234, 6789};
	unsigned char* pomocniCharNiz = 0;
	pomocniCharNiz = (unsigned char*)&noviIntNiz;				//C-like kastovanje
	for(int i = 0; i < 8; i++)
	{
		printf("Element %d, ima vrednost %0x\n", i, pomocniCharNiz[i]);
	}

	int nekiPromenljiviBroj = 44;
	int* promenljiviPointerNaPromenljiviBroj = &nekiPromenljiviBroj; 
	nekiPromenljiviBroj = 55;
	*promenljiviPointerNaPromenljiviBroj = 88;

	const int nekiKonstantniBroj = 33;
	//int* promenljiviPointerNaPromenljiviBroj = &nekiKonstantniBroj;	//CompileTime error - ne moze adresa konstante da se dodeli pointeru na promenljivu

	const int* promenljiviPointerNaKonstataniBroj = &nekiKonstantniBroj;
	//nekiKonstantniBroj = 33;											//ne moze se konstantan broj promeniti
	//*pointerNaKonstataniBroj = 99;									//ne moze ni preko pointera

	const int nekiNoviKonstantniBroj = 12;
	promenljiviPointerNaKonstataniBroj = &nekiNoviKonstantniBroj; //ovo radi zato sto je pointer promenljiv


	int* const konstantanPointerNaPromenljiviBroj = &nekiPromenljiviBroj;
	int nekiNoviPromenljiviBroj = 23;
	//konstantanPointerNaPromenljiviBroj = &nekiNoviPromenljiviBroj;		//CompileTime error konstantan pointer ne moze da pokazuje na drugu promenljivu
																			//tj. ne moze se dodeliti vrednost konstantni (izuzev u inicijalizaciji)

	const int zadnjiKonstantanBroj = 34;
	const int* const konstantanPointerNaKonstantniBroj = &zadnjiKonstantanBroj;

	const int* pointerNaKonstantuInicijalizovanPromenljivom = &nekiPromenljiviBroj;
	//*pointerNaKonstantuInicijalizovanPromenljivom = 45;					//Kad bi ovo moglo, mogli bismo konstantu promeniti preko pointera


	int obicanBroj = 44;
	int& referencaNaObicanBroj = obicanBroj;
	referencaNaObicanBroj++;												//operatori ne operisu nad referencama

	//int& referencaNaNekiBroj;												//CompileTime error referenca mora da se veze za neki objekat - da se inicijalizuje

	const int& referencaNaKonstantanBroj = 5;
	
	int* adresaReference = &referencaNaObicanBroj;							//Uzimanje adrese reference vraca adresu originalnog objekta (koga referenca referencira)
	
	//A evo malo i heap-a
	
	int* pointerNaHeap = new int(10);
	int* noviPointerNaHeap = new int;

	int** pointerNaPointerNaHeap = &pointerNaHeap;
	void* pointerNaHeapovskiPointer = static_cast<void*>(pointerNaHeap);

	return 0;
}

